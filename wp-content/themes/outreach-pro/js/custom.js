$(document).ready(function(){
    $(window).scroll(function(){
        if(window.scrollY < 20)
        {
            $(".nav-container").removeClass("fixed");
            $(".nav-container .logo-image").removeClass("logo-image-scroll");
        }
        else
        {
            $(".nav-container").addClass("fixed");
            $(".nav-container .logo-image").addClass("logo-image-scroll");
        }
    });

});