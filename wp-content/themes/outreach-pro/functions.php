<?php
//* Start the engine
include_once( get_template_directory() . '/lib/init.php' );

//* Set Localization (do not remove)
load_child_theme_textdomain( 'outreach', apply_filters( 'child_theme_textdomain', get_stylesheet_directory() . '/languages', 'outreach' ) );

//* Child theme (do not remove)
define( 'CHILD_THEME_NAME', __( 'Outreach Pro Theme', 'outreach' ) );
define( 'CHILD_THEME_URL', 'http://my.studiopress.com/themes/outreach/' );
define( 'CHILD_THEME_VERSION', '3.0.1' );

//* Add HTML5 markup structure
add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );

//* Add viewport meta tag for mobile browsers
add_theme_support( 'genesis-responsive-viewport' );

//* Enqueue Google fonts
add_action( 'wp_enqueue_scripts', 'outreach_google_fonts' );
function outreach_google_fonts() {

	wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Lato:400,700', array(), CHILD_THEME_VERSION );
	
}

//* Enqueue Responsive Menu Script
add_action( 'wp_enqueue_scripts', 'outreach_enqueue_responsive_script' );
function outreach_enqueue_responsive_script() {

	wp_enqueue_script( 'outreach-responsive-menu', get_bloginfo( 'stylesheet_directory' ) . '/js/responsive-menu.js', array( 'jquery' ), '1.0.0' );

}

//* Enqueue Responsive Menu animation css
add_action( 'wp_enqueue_scripts', 'outreach_enqueue_animation_style' );
function outreach_enqueue_animation_style() {

	wp_enqueue_style( 'animation-style', get_bloginfo( 'stylesheet_directory' ) . '/css/animate.css');

}

//* Add new image sizes
add_image_size( 'home-top', 1140, 460, TRUE );
add_image_size( 'home-bottom', 285, 160, TRUE );
add_image_size( 'sidebar', 300, 150, TRUE );

//* Add support for custom header
add_theme_support( 'custom-header', array(
	'header-selector' => '.site-title a',
	'header-text'     => false,
	'height'          => 100,
	'width'           => 340,
) );

//* Add support for custom background
add_theme_support( 'custom-background' );

//* Add support for additional color style options
add_theme_support( 'genesis-style-selector', array(
	'outreach-pro-blue' 	=>	__( 'Outreach Pro Blue', 'outreach' ),
	'outreach-pro-orange' 	=> 	__( 'Outreach Pro Orange', 'outreach' ),
	'outreach-pro-purple' 	=> 	__( 'Outreach Pro Purple', 'outreach' ),
	'outreach-pro-red' 		=> 	__( 'Outreach Pro Red', 'outreach' ),
) );

//* Add support for structural wraps
add_theme_support( 'genesis-structural-wraps', array(
	'header',
	'nav',
	'subnav',
	'site-inner',
	'footer-widgets',
	'footer',
) );

//* Add support for 4-column footer widgets
add_theme_support( 'genesis-footer-widgets', 4 );

//* Set Genesis Responsive Slider defaults
add_filter( 'genesis_responsive_slider_settings_defaults', 'outreach_responsive_slider_defaults' );
function outreach_responsive_slider_defaults( $defaults ) {

	$args = array(
		'location_horizontal'             => 'Left',
		'location_vertical'               => 'bottom',
		'posts_num'                       => '4',
		'slideshow_excerpt_content_limit' => '100',
		'slideshow_excerpt_content'       => 'full',
		'slideshow_excerpt_width'         => '35',
		'slideshow_height'                => '460',
		'slideshow_more_text'             => __( 'Continue Reading', 'outreach' ),
		'slideshow_title_show'            => 1,
		'slideshow_width'                 => '1140',
	);

	$args = wp_parse_args( $args, $defaults );
	
	return $args;
}

//* Hook after post widget after the entry content
add_action( 'genesis_after_entry', 'outreach_after_entry', 5 );
function outreach_after_entry() {

	if ( is_singular( 'post' ) )
		genesis_widget_area( 'after-entry', array(
			'before' => '<div class="after-entry widget-area">',
			'after'  => '</div>',
		) );

}

//* Modify the size of the Gravatar in the author box
add_filter( 'genesis_author_box_gravatar_size', 'outreach_author_box_gravatar_size' );
function outreach_author_box_gravatar_size( $size ) {

    return '80';
    
}

//* Remove comment form allowed tags
add_filter( 'comment_form_defaults', 'outreach_remove_comment_form_allowed_tags' );
function outreach_remove_comment_form_allowed_tags( $defaults ) {
	
	$defaults['comment_notes_after'] = '';
	return $defaults;

}
/*---logo on admin------*/
function custom_login_logo() {
    echo '<style type="text/css">
    h1 a { background-image: url('.content_url().'/uploads/2015/10/logo.png) !important;background-size: 180px 100px !important;width:200px !important;height: 100px !important;}
    </style>';
}
add_action('login_head', 'custom_login_logo');
function my_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
    return 'Cafe Suprime';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );

//* Add the sub footer section
add_action( 'genesis_before_footer', 'outreach_sub_footer', 5 );
function outreach_sub_footer() {

	if ( is_active_sidebar( 'sub-footer-left' ) || is_active_sidebar( 'sub-footer-right' ) ) {
		echo '<div class="sub-footer"><div class="wrap">';
		
		   genesis_widget_area( 'sub-footer-left', array(
		       'before' => '<div class="sub-footer-left">',
		       'after'  => '</div>',
		   ) );
	
		   genesis_widget_area( 'sub-footer-right', array(
		       'before' => '<div class="sub-footer-right">',
		       'after'  => '</div>',
		   ) );
	
		echo '</div><!-- end .wrap --></div><!-- end .sub-footer -->';	
	}
	
}

//* Register widget areas
genesis_register_sidebar( array(
	'id'          => 'home-top',
	'name'        => __( 'Home - Top', 'outreach' ),
	'description' => __( 'This is the top  of the Home page.', 'outreach' ),
) );
genesis_register_sidebar( array(
	'id'          => 'home-bottom',
	'name'        => __( 'Home - Bottom', 'outreach' ),
	'description' => __( 'This is the bottom  of the Home page.', 'outreach' ),
) );
genesis_register_sidebar( array(
	'id'          => 'after-entry',
	'name'        => __( 'After Entry', 'outreach' ),
	'description' => __( 'This is the after entry widget area.', 'outreach' ),
) );
genesis_register_sidebar( array(
	'id'          => 'sub-footer-left',
	'name'        => __( 'Sub Footer - Left', 'outreach' ),
	'description' => __( 'This is the left of the sub footer.', 'outreach' ),
) );
genesis_register_sidebar( array(
	'id'          => 'sub-footer-right',
	'name'        => __( 'Sub Footer - Right', 'outreach' ),
	'description' => __( 'This is the right of the sub footer.', 'outreach' ),
) );
genesis_register_sidebar( array(
	'id'          => 'before_header',

	'name'        => __( 'Before-header', 'outreach' ),

	'description' => __( 'This is the Before-header of the home page.', 'outreach' ),

) );
add_action( 'genesis_before_header', 'before_header_widget_area', 10 );
function before_header_widget_area() {
genesis_widget_area( 'before_header', array(
'before' => '<div class="before_header">',
'after' => '</div><div class="mywrap">',
) );
} 

genesis_register_sidebar( array(
	'id'          => 'after_footer',

	'name'        => __( 'After-footer', 'outreach' ),

	'description' => __( 'This is the After-footer of the home page.', 'outreach' ),

) );
add_action( 'genesis_after_footer', 'after_footer_widget_area', 10 );
function after_footer_widget_area() {
genesis_widget_area( 'after_footer', array(
'before' => '</div><div class="after_footer">',
'after' => '</div>',
) );
} 

genesis_register_sidebar( array(
	'id'          => 'before_content',

	'name'        => __( 'Before-content', 'outreach' ),

	'description' => __( 'This is the Before-content of the home page.', 'outreach' ),

) );
add_action( 'genesis_before_content', 'before_content_widget_area', 5 );
function before_content_widget_area() {
genesis_widget_area( 'before_content', array(
'before' => '<div class="before_content">',
'after' => '</div>',
) );
} 

/** Customize Genesis Footer */
/** This snippet will help to modify the complete genesis footer area */
remove_action( 'genesis_footer', 'genesis_do_footer' );

genesis_register_sidebar( array(
	'id'          => 'genesis_footer',

	'name'        => __( 'genesis_Copyright', 'outreach' ),

	'description' => __( 'This is the genesis_footer of the home page.', 'outreach' ),

) );
add_action( 'genesis_footer', 'wpvkp_footer' );
function wpvkp_footer() {
    
    genesis_widget_area( 'genesis_footer', array(
   'before' => '<div class="genesis_footer">',
    'after' => '</div>',
) );
} 